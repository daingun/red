# Red

## Description
Ed editor porting to Rust

## Usage
```
Usage: red FILE

Options:
    -h, --help          print this help menu
    -p, --prompt STRING use STRING as an interactive prompt
```

## Available commands
Currently a command must be supplied. No command will result in a error.

    '(.)a'
Append text after the addressed line. To exit append mode type a line with only '.' . The current address is set to the last line appended.

    '(.,.)c'
Change addressed lines, i.e. lines are deleted and program goes into insert mode. The current address is set to the last line inserted.
The copied lines are stored in the yank buffer.

    '(.,.)d'
Delete addressed lines. The current address is set to next available line or the last line of the file.
The copied lines are stored in the yank buffer.

    '(.)i'
Insert text before the addressed line. To exit insert mode type a line with only '.' . The current address is set to the last line inserted.

    '(.,.)m(.)'
Move addressed lines after the right hand address. The current address is set to the last line moved.
The copied lines are stored in the yank buffer.

    '(.,.)n'
Print addressed lines with line numbers.

    '(.,.)p'
Print addressed lines.

    'P'
Toggles the command prompt on and off. Unless a prompt is specified with command-line option '-p', the command prompt is by default turned off.

    'q'
Quit red without saving.

    '(.,.)t(.)'
Copy addressed lines after the right hand address. The current address is set to the last line moved.
The copied lines are stored in the yank buffer.

    'w'
Write the buffer into the file.

    '(.)x'
Put the content of the yank buffer after the given address. The current address is set to the last line insered.

    '(.,.)y'
Yanks the addressed lines into the yank buffer. The current address is not modified.
The yank buffer is overwritten by the following commands: c, d, m, t, y.

## Ranges
When the address are separated by a semi-column the current address is set to the first address before the second address is calculated.

    '0'
Where it make sense '0' means 'before first line' (e.g. insert, move).

    '$'
The last line.

    '.'
The current line.

    ',' or '%'
The entire buffer. It is equivalent to '1,$'.

    ';'
The buffer from current address to last line. It is equivalent to '.,$'.

    '/RE/'
The next line containing the regular expression RE. The search wraps to the beginning of the buffer and continues down to the current line, if necessary.

    '?RE?'
The previous line containing the regular expression RE. The search wraps to the end of the buffer and continues down to the current line, if necessary.

    '+N'
The Nth next line. N is a non-negative number.

    '-N'
The Nth previous line. N is a non-negative number.

    '+'
The next line. It is equivalent to '+1'.

    '-'
The previous line. It is equivalent to '-1'.
