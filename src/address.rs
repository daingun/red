use std::fmt;
use std::fmt::{Display, Formatter};

/// Structure that describes an address
#[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub struct Address(usize);

impl Address {
    #[must_use]
    /// Create a new address
    ///
    /// # Arguments
    ///
    /// * `address` - address to be the current address
    pub fn new(address: usize) -> Self {
        Self(address)
    }

    /// Increment the current address by one.
    pub(crate) fn increment_mut(&mut self) {
        self.0 += 1;
    }

    /// Increment the address by `n`.
    pub(crate) fn increment_by(self, n: usize) -> Self {
        Self(self.0 + n)
    }

    /// Decrement the address by `n`.
    pub(crate) fn decrement_by(self, n: usize) -> Self {
        Self(self.0 - n)
    }

    /// Get the address following the current one.
    pub(crate) fn next(self) -> Self {
        Address(self.0 + 1)
    }

    /// Get the address preceding the current one.
    pub(crate) fn previous(self) -> Self {
        Address(self.0 - 1)
    }

    /// Transform the address into the index of the vector buffer.
    pub(crate) fn to_index(self) -> usize {
        self.0 - 1
    }
}

impl std::ops::Sub for Address {
    type Output = usize;
    fn sub(self, rhs: Self) -> Self::Output {
        self.0 - rhs.0
    }
}

impl Display for Address {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

// start, inclusive end
pub(crate) struct AddressInclusiveRange {
    start: Address,
    stop: Address,
}

impl AddressInclusiveRange {
    pub(crate) fn new(start: Address, stop: Address) -> Self {
        Self { start, stop }
    }
}

impl Iterator for AddressInclusiveRange {
    type Item = Address;

    #[inline]
    fn next(&mut self) -> Option<Address> {
        if self.start <= self.stop {
            let v = self.start;
            self.start.increment_mut();
            Some(v)
        } else {
            None
        }
    }
}

/// Structure that contains all the addresses.
pub struct Addresses {
    /// First address
    pub(crate) first_addr: Option<Address>,
    /// Second address
    pub(crate) second_addr: Option<Address>,
    /// Current address
    current_addr: Address,
    /// Last address
    last_addr: Address,
}

impl Addresses {
    /// Returns a new Address structure.
    pub(crate) fn new() -> Self {
        Addresses {
            first_addr: Some(Address::new(0)),
            second_addr: Some(Address::new(0)),
            current_addr: Address::new(0),
            last_addr: Address::new(0),
        }
    }

    /// Returns the current address.
    #[must_use]
    pub(crate) fn current_addr(&self) -> Address {
        self.current_addr
    }

    /// Increases the current address by one.
    /// Checks if the current line is the last line.
    #[allow(dead_code)]
    pub(crate) fn inc_current_addr(&mut self) -> Address {
        if (self.current_addr.next()) > self.last_addr {
            self.current_addr = self.last_addr
        } else {
            self.current_addr.increment_mut();
        }
        self.current_addr
    }

    /// Sets the current address.
    ///
    /// # Arguments
    ///
    /// * `address` - address to be the current address
    pub(crate) fn set_current_addr(&mut self, address: Address) {
        self.current_addr = address;
    }

    /// Returns the last address.
    #[must_use]
    pub(crate) fn last_addr(&self) -> Address {
        self.last_addr
    }

    /// Sets the last address.
    ///
    /// # Arguments
    ///
    /// * `address` - address to be the last address
    pub(crate) fn set_last_addr(&mut self, address: Address) {
        self.last_addr = address;
    }

    /// Set the first address.
    ///
    /// # Arguments
    ///
    /// * `address` - address to be used as first address
    pub(crate) fn set_first_addr(&mut self, address: Address) {
        self.first_addr = Some(address);
    }

    /// Increment the second address
    ///
    /// # Arguments
    ///
    /// * `n` - number to increment the second address
    pub(crate) fn increment_second_addr(&mut self, n: usize) {
        self.second_addr = self.second_addr.map(|a| a.increment_by(n));
    }

    /// Decrement the second address
    ///
    /// # Arguments
    ///
    /// * `n` - number to decrement the second address
    pub(crate) fn decrement_second_addr(&mut self, n: usize) {
        if self.second_addr.map_or(false, |a| a > Address::new(n)) {
            self.second_addr = self.second_addr.map(|a| a.decrement_by(n));
        }
    }

    /// Set the second address.
    ///
    /// # Arguments
    ///
    /// * `address` - address to be used as second address
    pub(crate) fn set_second_addr(&mut self, address: Address) {
        self.second_addr = Some(address);
    }
}
