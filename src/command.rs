/*
 *Red, a line editor derived from ed editor
 *Copyright (C) 2017-2019  Walter Bruschi
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *CREDITS
 *This program is derived from Free Software Foundation version of ed.
 *This program has been modified to be written in Rust instead of C.
 *Any credit to the original work is theirs.
 *Copyright (C) 1993, 1994, 2006-2017 Free Software Foundation, Inc.
 *The original work can be found at the following website:
 *https://www.gnu.org/software/ed/
 */

use std::cell::Cell;
use std::cmp;
use std::fs::{File, OpenOptions};
use std::io::Write;

use crate::address::{Address, AddressInclusiveRange};
use crate::error::Error;
use crate::iofs;
use crate::parse::Red;

/// Trait that defines the behaviour of a command
pub(crate) trait Command {
    /// Execute the command
    ///
    /// # Arguments
    ///
    /// * `red` - structure with addresses and error message
    fn execute(&self, red: &mut Red) -> Result<(), Error>;
}

/// Trait that defines the behaviour of a command that can be undone
pub(crate) trait InvertibleCommand {
    /// Execute the command
    ///
    /// # Arguments
    ///
    /// * `red` - structure with addresses and error message
    fn invert(&self, red: &mut Red) -> Result<(), Error>;
}

/// No operations, always successful.
pub(crate) struct Empty {
    _priv: (),
}

impl Empty {
    /// Create an `Empty` command.
    pub(crate) fn new() -> Self {
        Self { _priv: () }
    }
}

impl Command for Empty {
    fn execute(&self, _red: &mut Red) -> Result<(), Error> {
        Ok(())
    }
}

/// Prints the file buffer vector elements from the first element `first_line`
/// to the last element `last_line`. Optionally print line numbers.
pub(crate) struct PrintLines {
    first_line: Address,
    last_line: Address,
    print_line_numbers: bool,
}

impl PrintLines {
    /// Create a `PrintLines` command.
    ///
    /// # Arguments
    ///
    /// * `first_line` - first line of the range
    /// * `last_line` - last line of the range
    /// * `print_line_numbers` - print line numbers if true
    pub(crate) fn new(first_line: Address, last_line: Address, print_line_numbers: bool) -> Self {
        Self {
            first_line,
            last_line,
            print_line_numbers,
        }
    }
}

impl Command for PrintLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        if self.print_line_numbers {
            for line in AddressInclusiveRange::new(self.first_line, self.last_line) {
                println!("{}\t{}", line, red.line(line));
            }
        } else {
            for line in AddressInclusiveRange::new(self.first_line, self.last_line) {
                println!("{}", red.line(line));
            }
        }
        red.addresses.set_current_addr(self.last_line);
        Ok(())
    }
}

/// Command ot append lines to the file buffer.
pub(crate) struct InsertLine {
    address: Address,
    append: bool,
}

impl InsertLine {
    /// Create an `InsertLine` command.
    ///
    /// # Arguments
    ///
    /// * `address` - address at which insertion happens
    /// * `append` - true for append, false for insert
    pub(crate) fn new(address: Address, append: bool) -> Self {
        Self { address, append }
    }
}

impl Command for InsertLine {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        let mut insertion_address = if self.append {
            self.address.next()
        } else {
            self.address
        };

        loop {
            match iofs::get_input() {
                Ok(mut text) => {
                    // remove trailing newline
                    // TODO use trim_end_matches, maybe in get_input
                    text.pop();
                    if text == "." {
                        break;
                    }
                    red.insert(insertion_address, text);
                    insertion_address.increment_mut();
                    red.is_modified = true;
                }
                Err(e) => {
                    println!("{}", e);
                    continue;
                }
            };
        }
        red.addresses.set_current_addr(insertion_address.previous());
        red.addresses
            .set_last_addr(Address::new(red.number_of_lines()));
        Ok(())
    }
}

/// Command that deletes line range.
pub(crate) struct DeleteLines {
    first_line: Address,
    last_line: Address,
    deleted_lines: Cell<Vec<String>>,
}

impl DeleteLines {
    /// Create a `DeleteLines` command.
    ///
    /// # Arguments
    ///
    /// * `first_line` - first line of the range
    /// * `last_line` - last line of the range
    pub(crate) fn new(first_line: Address, last_line: Address) -> Self {
        Self {
            first_line,
            last_line,
            deleted_lines: Cell::new(vec![]),
        }
    }
}

impl Command for DeleteLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        // Save the deleted lines inside the command and in the yank buffer.
        let deleted_lines = red.remove_block(self.first_line, self.last_line);
        self.deleted_lines.set(deleted_lines.clone());
        // No need to clear yank_buffer it is substituted.
        red.yank_buffer = deleted_lines;

        red.addresses
            .set_last_addr(Address::new(red.number_of_lines()));
        red.addresses
            .set_current_addr(cmp::min(self.first_line, red.addresses.last_addr()));
        red.addresses.second_addr = Some(red.addresses.current_addr());
        red.is_modified = true;
        Ok(())
    }
}

impl InvertibleCommand for DeleteLines {
    fn invert(&self, red: &mut Red) -> Result<(), Error> {
        red.addresses.set_current_addr(self.first_line);
        Ok(())
    }
}

/// Command that moves lines.
pub(crate) struct MoveLines {
    first_line: Address,
    last_line: Address,
    move_to: Address,
}

impl MoveLines {
    /// Create a `MoveLines` command.
    ///
    /// # Arguments
    ///
    /// * `first_line` - first line of the range
    /// * `last_line` - last line of the range
    /// * `move_to` - address after where to move the text
    pub(crate) fn new(first_line: Address, last_line: Address, move_to: Address) -> Self {
        Self {
            first_line,
            last_line,
            move_to,
        }
    }
}

impl Command for MoveLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        // No need to clear yank_buffer it is substituted.
        red.yank_buffer = red.remove_block(self.first_line, self.last_line);

        red.insert_block(self.move_to.next(), red.yank_buffer.clone());

        let range = self.last_line - self.first_line + 1; // number of moved lines
        red.addresses
            .set_current_addr(self.move_to.increment_by(range));
        red.is_modified = true;
        Ok(())
    }
}

/// Command that copies lines.
pub(crate) struct CopyLines {
    first_line: Address,
    last_line: Address,
    copy_to: Address,
}

impl CopyLines {
    /// Create a `CopyLines` command.
    ///
    /// # Arguments
    ///
    /// * `first_line` - first line of the range
    /// * `last_line` - last line of the range
    /// * `copy_to` - address after where to copy the text
    pub(crate) fn new(first_line: Address, last_line: Address, copy_to: Address) -> Self {
        Self {
            first_line,
            last_line,
            copy_to,
        }
    }
}

impl Command for CopyLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        red.yank_buffer.clear();
        for line in AddressInclusiveRange::new(self.first_line, self.last_line) {
            red.yank_buffer.push(red.line(line).clone());
        }

        red.insert_block(self.copy_to.next(), red.yank_buffer.clone());

        red.addresses
            .set_last_addr(Address::new(red.number_of_lines()));
        let range = self.last_line - self.first_line + 1; // number of moved lines
        red.addresses
            .set_current_addr(self.copy_to.increment_by(range));
        red.is_modified = true;
        Ok(())
    }
}

/// Command that yanks lines to the yank buffer
pub(crate) struct YankLines {
    first_line: Address,
    last_line: Address,
}

impl YankLines {
    /// Create a `YankLines` command.
    ///
    /// # Arguments
    ///
    /// * `first_line` - first line of the range
    /// * `last_line` - last line of the range
    pub(crate) fn new(first_line: Address, last_line: Address) -> Self {
        Self {
            first_line,
            last_line,
        }
    }
}

impl Command for YankLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        red.yank_buffer.clear();
        for line in AddressInclusiveRange::new(self.first_line, self.last_line) {
            red.yank_buffer.push(red.line(line).clone());
        }
        Ok(())
    }
}

/// Command that puts lines from yank buffer.
pub(crate) struct PutLines {
    line: Address,
}

impl PutLines {
    /// Create a `PutLines` command.
    ///
    /// # Arguments
    ///
    /// * `line` - address after where to put the text
    pub(crate) fn new(line: Address) -> Self {
        Self { line }
    }
}

impl Command for PutLines {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        if red.yank_buffer.is_empty() {
            red.set_error_msg(Error::NothingPut);
            return Err(Error::NothingPut);
        }

        red.insert_block(self.line.next(), red.yank_buffer.clone());

        red.addresses
            .set_last_addr(Address::new(red.number_of_lines()));
        red.addresses
            .set_current_addr(self.line.increment_by(red.yank_buffer.len()));
        red.is_modified = true;
        Ok(())
    }
}

/// Command that prints errors.
pub(crate) struct PrintError {
    _priv: (),
}

impl PrintError {
    /// Create a `PrintError` command.
    pub(crate) fn new() -> Self {
        Self { _priv: () }
    }
}

impl Command for PrintError {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        if let Some(err) = red.get_error_msg() {
            println!("{}", err);
        } else {
            println!("No error");
        }
        Ok(())
    }
}

/// Command that writes `file_buffer` to a `BufWriter`.
pub(crate) struct WriteToBuffer {
    _priv: (),
}

impl WriteToBuffer {
    /// Create a `WriteToBuffer` command.
    pub(crate) fn new() -> Self {
        Self { _priv: () }
    }
}

impl Command for WriteToBuffer {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        // Automatically writes to file when BufWriter is dropped.

        // .write(true): If the file already exists, any write calls on it
        // will overwrite its contents, without truncating it.
        // .truncate(true): truncate the file to 0 length to clean it.
        let file: File = OpenOptions::new()
            .write(true)
            .truncate(true)
            .open(&red.filename)
            .map_err(Error::IO)?;
        let mut buffer = iofs::create_writer(file);

        for line in &red.file_buffer {
            // if the string is not cloned the modifications are done on the
            // current file_buffer
            let mut line_to_insert = line.clone();
            line_to_insert.push('\n');
            buffer
                .write_all(line_to_insert.as_bytes())
                .map_err(Error::IO)?;
        }
        red.is_modified = false;
        Ok(())
    }
}

/// Command that switch the prompt on and off.
pub(crate) struct SwitchPrompt {
    _priv: (),
}

impl SwitchPrompt {
    /// Create a `SwitchPrompt` command.
    pub(crate) fn new() -> Self {
        Self { _priv: () }
    }
}

impl Command for SwitchPrompt {
    fn execute(&self, red: &mut Red) -> Result<(), Error> {
        red.prompt_on = !red.prompt_on;
        Ok(())
    }
}
