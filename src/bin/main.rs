/*
 *Red, a line editor derived from ed editor
 *Copyright (C) 2017-2019  Walter Bruschi
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *CREDITS
 *This program is derived from Free Software Foundation version of ed.
 *This program has been modified to be written in Rust instead of C.
 *Any credit to the original work is theirs.
 *Copyright (C) 1993, 1994, 2006-2017 Free Software Foundation, Inc.
 *The original work can be found at the following website:
 *https://www.gnu.org/software/ed/
 */

use std::{
    env,
    io::{self, Write},
};

use getopts::Options;

use red::{
    error::Error,
    iofs,
    parse::{self, Red, ReturnCommand},
};

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    // optional option that does not take an argument
    opts.optflag("h", "help", "print this help menu");
    // optional option that takes an argument
    opts.optopt(
        "p",
        "prompt",
        "use STRING as an interactive prompt",
        "STRING",
    );
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(e) => {
            println!("{}", e);
            return Ok(());
        }
    };

    if matches.opt_present("h") {
        print_usage(&program, &opts);
        return Ok(());
    }

    let prompt = matches.opt_str("p").unwrap_or_else(|| "*".to_string());

    let filename = if matches.free.is_empty() {
        print_usage(&program, &opts);
        return Ok(());
    } else {
        matches.free[0].clone()
    };

    let (reader, length) = iofs::create_reader(&filename)?;
    println!("{}", length);

    let mut red = Red::new(reader, length, filename, matches.opt_present("p"));

    loop {
        if red.prompt_on {
            print!("{}", prompt);
        }
        // print line immediately
        io::stdout().flush().expect("cannot flush the stdout");

        let mut input = match iofs::get_input() {
            Ok(i) => i,
            Err(e) => {
                println!("{}", e);
                continue;
            }
        };

        match parse::exec_command(&mut red, &mut input) {
            ReturnCommand::Quit => return Ok(()),
            ReturnCommand::Error => println!("?"),
            ReturnCommand::ProvideCommand => println!("Please provide a command"),
            ReturnCommand::NoAvailableCommand(c) => println!("No available command: {}", c),
            ReturnCommand::Done => {
                //red.print();
                continue;
            }
        };
    }
}

/// Prints the list of the options of the executable
///
/// # Arguments
///
/// * `program` - name of the executable
/// * `opts` - option list from getopts
fn print_usage(program: &str, opts: &Options) {
    let brief = format!("Usage: {} FILE", program);
    print!("{}", opts.usage(&brief));
}
