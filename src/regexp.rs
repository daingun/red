/*
 *Red, a line editor derived from ed editor
 *Copyright (C) 2017-2019  Walter Bruschi
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *CREDITS
 *This program is derived from Free Software Foundation version of ed.
 *This program has been modified to be written in Rust instead of C.
 *Any credit to the original work is theirs.
 *Copyright (C) 1993, 1994, 2006-2017 Free Software Foundation, Inc.
 *The original work can be found at the following website:
 *https://www.gnu.org/software/ed/
 */

use regex::Regex;

use crate::address::Address;
use crate::error::Error;
use crate::parse::Red;

/// Return the address of the next line matching a regular expression in a
/// given direction. wrap around begin/end of editor buffer if necessary.
///
/// # Arguments
///
/// * `red` - structure holding addresses and error data
/// * `input` - input string that is converted into a regex
/// * `forward` - direction of the search
pub(crate) fn match_addr(
    red: &mut Red,
    input: &mut String,
    forward: bool,
) -> Result<Address, Error> {
    let pattern = get_pattern(input)?;

    let exp = match Regex::new(&pattern) {
        Ok(re) => re,
        Err(_) => return Err(Error::Generic("Failed to create regex".to_string())),
    };

    let mut addr = red.addresses.current_addr();

    loop {
        addr = if forward {
            addr.next()
        } else {
            addr.previous()
        };
        // wrap around buffer
        addr = if red.addresses.last_addr() < addr {
            Address::new(1)
        } else {
            addr
        };
        addr = if addr == Address::new(0) {
            red.addresses.last_addr()
        } else {
            addr
        };

        if exp.is_match(red.line(addr).as_str()) {
            return Ok(addr);
        }

        if red.addresses.current_addr() == addr {
            break;
        }
    }

    red.set_error_msg(Error::NoMatch);
    Err(Error::NoMatch)
}

/// Extracts the string between '/' or '?', and shorten the input command.
///
/// # Arguments
///
/// * `string` - input command
fn get_pattern(string: &mut String) -> Result<String, Error> {
    let mut pattern = String::new();
    let mut rest = String::new();

    let delimiter = string.chars().next().ok_or(Error::EmptyInput)?;
    string.remove(0);
    let mut stop: bool = false;
    for c in string.chars() {
        if c != '\n' && c != delimiter && !stop {
            pattern.push(c);
        } else {
            rest.push(c);
            stop = true;
        }
    }
    *string = rest;
    Ok(pattern)
}

// Crate regular expression to split input line in usable
// addresses and command.
// NOT IN USE BUT IT MAY BE A FUTURE REFERENCE FOR REGEX
//pub fn parse_input(input: &str) -> Input {
//let re = Regex::new(r"(^\d*),?(\d*)(%?[a-zA-Z]*)(\d*)")
//.expect("cannot crate regexp");
//// regex explanation:
//// (^\d*) zero or more digit at the start of the line -> &token[1]
//// ,? zero or one comma
//// (\d*) zero or more digit -> &token[2]
//// (%?[a-zA-Z]*) zero or more char with zero or one % -> &token[3]
//// (\d*) zero or more digit -> &token[4]

//let tokens = re.captures(input)
//.expect("unable to split command");

//let start = convert_to_int(tokens.get(1)
//.expect("cannot split the regex")
//.as_str());
//let end = convert_to_int(tokens.get(2)
//.expect("cannot split the regex")
//.as_str());
//let command = tokens.get(3)
//.expect("cannot split the regex")
//.as_str();

//let move_to = convert_to_int(tokens.get(4)
//.expect("cannot split the regex")
//.as_str());

//Input::new(start, end, command, move_to)
//}
