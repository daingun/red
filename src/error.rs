use std::error;
use std::fmt;
use std::io;
use std::num::{ParseIntError, TryFromIntError};

#[derive(Debug)]
pub enum Error {
    /// Generic error with a string.
    Generic(String),
    /// The address is invalid.
    InvalidAddress,
    /// The destination of the command is invalid.
    InvalidDestination,
    /// The destination of the command is missing.
    MissingDestination,
    /// There is nothing to put from the yank buffer.
    NothingPut,
    /// The regex gives no match.
    NoMatch,
    /// Input/Output errors.
    IO(io::Error),
    /// Integer parsing error.
    ParseInt(ParseIntError),
    /// File is too big.
    FileTooBig(TryFromIntError),
    /// Input is empty
    EmptyInput,
}

/// Display implementation for `Error`.
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            Error::Generic(msg) => write!(f, "{}", msg),
            Error::InvalidAddress => write!(f, "Invalid address"),
            Error::InvalidDestination => write!(f, "Invalid destination"),
            Error::MissingDestination => write!(f, "Destination expected"),
            Error::NothingPut => write!(f, "Nothing to put"),
            Error::NoMatch => write!(f, "No match"),
            Error::IO(err) => write!(f, "{}", err),
            Error::ParseInt(_) => write!(f, "Cannot parse address"),
            Error::FileTooBig(_) => write!(f, "File is too big"),
            Error::EmptyInput => write!(f, "Input is empty"),
        }
    }
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match &self {
            Error::IO(ref err) => err.source(),
            Error::ParseInt(ref err) => err.source(),
            Error::FileTooBig(ref err) => err.source(),
            _ => None,
        }
    }
}

impl PartialEq for Error {
    #[allow(clippy::match_same_arms)]
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Error::Generic(msg1), Error::Generic(msg2)) => msg1 == msg2,
            (Error::InvalidAddress, Error::InvalidAddress) => true,
            (Error::InvalidDestination, Error::InvalidDestination) => true,
            (Error::MissingDestination, Error::MissingDestination) => true,
            (Error::NothingPut, Error::NothingPut) => true,
            (Error::NoMatch, Error::NoMatch) => true,
            (Error::IO(_), Error::IO(_)) => true,
            (Error::ParseInt(_), Error::ParseInt(_)) => true,
            (Error::FileTooBig(_), Error::FileTooBig(_)) => true,
            (Error::EmptyInput, Error::EmptyInput) => true,
            (_, _) => false,
        }
    }
}
