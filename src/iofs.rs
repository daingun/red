/*
 *Red, a line editor derived from ed editor
 *Copyright (C) 2017-2019  Walter Bruschi
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *CREDITS
 *This program is derived from Free Software Foundation version of ed.
 *This program has been modified to be written in Rust instead of C.
 *Any credit to the original work is theirs.
 *Copyright (C) 1993, 1994, 2006-2017 Free Software Foundation, Inc.
 *The original work can be found at the following website:
 *https://www.gnu.org/software/ed/
 */

use std::convert::TryFrom;
use std::fs::File;
use std::io::{self, BufRead, BufReader, BufWriter};
use std::path::Path;

use crate::error::Error;

/// Reads a file and returns a File type
///
/// # Arguments
///
/// * `filename` - path to the file on the disk
///
/// # Errors
///
/// Returns an error if a new file cannot be created.
fn read_file<P>(filename: &P) -> Result<File, Error>
where
    P: AsRef<Path>,
{
    if let Ok(file) = File::open(filename) {
        Ok(file)
    } else {
        println!("Creating new file...");
        match File::create(filename) {
            Ok(new_file) => Ok(new_file),
            Err(why) => Err(Error::IO(why)),
        }
    }
}

/// Retrieves the length from the metadata of a file in bytes.
///
/// # Arguments
///
/// * `file` - file structure
///
/// # Errors
///
/// Returns an error it is not possible to obtain the file length.
fn file_length(file: &File) -> Result<usize, Error> {
    match file.metadata() {
        Ok(data) => usize::try_from(data.len()).map_err(Error::FileTooBig),
        Err(why) => Err(Error::IO(why)),
    }
}

/// Create a new buffer to store the file and the lenth of the file.
/// This avoids to read the file multiple times.
/// Returns a tuple `(BufReader, usize)`.
///
/// # Arguments
///
/// * `filename` - path to the file on the disk
///
/// # Errors
///
/// Returns an error if a new reader buffer cannot be created.
pub fn create_reader<P>(filename: &P) -> Result<(BufReader<File>, usize), Error>
where
    P: AsRef<Path>,
{
    let file = read_file(&filename)?;
    let length = file_length(&file)?;
    Ok((BufReader::with_capacity(length, file), length))
}

/// Store the buffer in a vector of String;
/// every element of the vector is a line of the buffer.
/// The index of the vector plus 1 is the line number.
///
/// # Arguments
///
/// * `buffer` - reader buffer
/// * `length` - length of the file to preallocate the vector
/// TODO - the length passed is the size in bytes of the file,
///        the vector shall have less elements, i.e. lines,
///        but at least is big enough to postpone reallocation.
///        At the moment I divide the file size by 40 bytes per line.
///        This may cause reallocation near the pushing of the end of the file.
#[must_use]
pub(crate) fn store_buffer(buffer: BufReader<File>, length: usize) -> Vec<String> {
    let length = length / 40;
    let mut vector: Vec<String> = Vec::with_capacity(length);
    for line in buffer.lines() {
        match line {
            Err(_) => return vec![],
            Ok(l) => vector.push(l),
        }
    }
    vector
}

/// Create a new buffer to write the file to. This avoids to write the file
/// multiple times. Returns a `BufWriter`
///
/// # Arguments
///
/// * `file` - File structure
#[must_use]
pub fn create_writer(file: File) -> BufWriter<File> {
    BufWriter::new(file)
}

/// Reads the input from the command line and return it as a String
/// inside a Result or forwards an `Error`.
///
/// # Errors
///
/// Returns and error if standard input cannot be read.
pub fn get_input() -> Result<String, Error> {
    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_) => Ok(input),
        Err(why) => Err(Error::IO(why)),
    }
}
