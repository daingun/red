/*
 *Red, a line editor derived from ed editor
 *Copyright (C) 2017-2019  Walter Bruschi
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU General Public License as published by
 *the Free Software Foundation, either version 3 of the License, or
 *(at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU General Public License for more details.
 *
 *You should have received a copy of the GNU General Public License
 *along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *CREDITS
 *This program is derived from Free Software Foundation version of ed.
 *This program has been modified to be written in Rust instead of C.
 *Any credit to the original work is theirs.
 *Copyright (C) 1993, 1994, 2006-2017 Free Software Foundation, Inc.
 *The original work can be found at the following website:
 *https://www.gnu.org/software/ed/
 */

use crate::address::{Address, Addresses};
use crate::command::{
    Command, CopyLines, DeleteLines, Empty, InsertLine, MoveLines, PrintError, PrintLines,
    PutLines, SwitchPrompt, WriteToBuffer, YankLines,
};
use crate::error::Error;
use crate::iofs;
use crate::regexp;

use std::{fs::File, io::BufReader};

/// Global configuration holding addresses, errors.
/// This is to avoid global static variables.
pub struct Red {
    /// filename
    pub filename: String,
    /// file buffer
    pub file_buffer: Vec<String>,
    /// Addresses
    pub addresses: Addresses,
    /// Is the buffer modified from last save
    pub is_modified: bool,
    /// Is prompt on?
    pub prompt_on: bool,
    /// Error message
    pub errmsg: Option<Error>,
    /// Yank buffer
    pub yank_buffer: Vec<String>,
}

impl Red {
    /// Create a new Red structure.
    ///
    /// # Arguments
    ///
    /// * `buffer` - reader buffer
    /// * `length` - length of the file to preallocate the vector
    /// * `filename` - name of the file
    /// * `prompt_on` - true if the prompt is activated
    #[must_use]
    pub fn new(reader: BufReader<File>, length: usize, filename: String, prompt_on: bool) -> Self {
        let buffer = iofs::store_buffer(reader, length);
        let mut red = Red {
            filename,
            file_buffer: buffer,
            addresses: Addresses::new(),
            is_modified: false,
            prompt_on,
            errmsg: None,
            yank_buffer: vec![],
        };
        let file_size = red.file_buffer.len();
        red.addresses.set_last_addr(Address::new(file_size));
        // set current line to last line
        red.addresses.set_current_addr(Address::new(file_size));
        red
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        println!(
            "fa {:?}, sa {:?}, ca {:?}, la {:?}, e {:?}",
            self.addresses.first_addr,
            self.addresses.second_addr,
            self.addresses.current_addr(),
            self.addresses.last_addr(),
            self.get_error_msg()
        );
    }

    /// Sets the error message.
    ///
    /// # Arguments
    ///
    /// * `err` - error to be stored
    pub fn set_error_msg(&mut self, err: Error) {
        self.errmsg = Some(err);
    }

    /// Sets a generic error with string.
    ///
    /// # Arguments
    ///
    /// * `msg` - message to be stored
    pub fn set_generic_error<S: Into<String>>(&mut self, msg: S) {
        self.errmsg = Some(Error::Generic(msg.into()));
    }

    /// Returns the error message.
    #[must_use]
    pub fn get_error_msg(&self) -> &Option<Error> {
        &self.errmsg
    }

    /// Set default range and return Ok() if address range is valid.
    ///
    /// # Arguments
    ///
    /// * `self` - structure with addresses and error message
    /// * `n` - first address
    /// * `m` - second address
    /// * `addr_cnt` - number of addresses
    ///
    /// # Errors
    ///
    /// Returns an error if the address is invalid
    fn check_addr_range(&mut self, n: Address, m: Address, addr_cnt: i8) -> Result<(), Error> {
        if addr_cnt == 0 {
            self.addresses.set_first_addr(n);
            self.addresses.set_second_addr(m);
        }
        if self.addresses.first_addr.is_none()
            || self.addresses.first_addr < Some(Address::new(1))
            || self.addresses.first_addr > self.addresses.second_addr
            || self.addresses.second_addr > Some(self.addresses.last_addr())
        {
            self.set_error_msg(Error::InvalidAddress);
            return Err(Error::InvalidAddress);
        }
        Ok(())
    }

    /// Set defaults to `current_addr` and return `Ok()` if address range is valid.
    ///
    /// # Arguments
    ///
    /// * `self` - structure with addresses and error message
    /// * `addr_cnt` - number of addresses
    ///
    /// # Errors
    ///
    /// Returns an error if the address is invalid
    pub fn check_addr_range2(&mut self, addr_cnt: i8) -> Result<(), Error> {
        let current_addr = self.addresses.current_addr();
        self.check_addr_range(current_addr, current_addr, addr_cnt)
    }

    /// Set default `addresses.second_addr` and return `Ok()`
    /// if `addresses.second_addr` is valid.
    ///
    /// # Arguments
    ///
    /// * `self` - structure with addresses and error message
    /// * `addr` - address to be checked
    /// * `addr_cnt` - number of addresses
    fn check_second_addr(&mut self, addr: Address, addr_cnt: i8) -> Result<(), Error> {
        if 0 == addr_cnt {
            self.addresses.set_second_addr(addr);
        }
        if self.addresses.second_addr.is_none()
            || self.addresses.second_addr < Some(Address::new(1))
            || self.addresses.second_addr > Some(self.addresses.last_addr())
        {
            self.set_error_msg(Error::InvalidAddress);
            return Err(Error::InvalidAddress);
        }
        Ok(())
    }

    /// Return the last line number.
    #[must_use]
    pub fn last_addr(&self) -> usize {
        self.file_buffer.len()
    }

    /// Get a reference of the line at `address`
    pub(crate) fn line(&self, address: Address) -> &String {
        &self.file_buffer[address.to_index()]
    }

    /// Insert a line at `address` shifting all lines after it to the right
    pub(crate) fn insert(&mut self, address: Address, line: String) {
        self.file_buffer.insert(address.to_index(), line);
    }

    /// Insert a block of lines at the given `address` shifting all lines after
    /// it to the right
    pub(crate) fn insert_block(&mut self, address: Address, block: Vec<String>) {
        let index = address.to_index();
        self.file_buffer.splice(index..index, block);
    }

    /// Remove and return a block of lines from `start` to `end` included
    pub(crate) fn remove_block(&mut self, start: Address, end: Address) -> Vec<String> {
        self.file_buffer
            .drain(start.to_index()..=end.to_index())
            .collect()
    }

    /// Return the number of lines of the file buffer
    pub(crate) fn number_of_lines(&self) -> usize {
        self.file_buffer.len()
    }
}

impl Default for Red {
    /// Create the default value for Red.
    fn default() -> Self {
        Red {
            filename: "".to_string(),
            file_buffer: vec![],
            addresses: Addresses::new(),
            is_modified: false,
            prompt_on: false,
            errmsg: None,
            yank_buffer: vec![],
        }
    }
}

/// Split a string of type "10test" into a number and the rest.
/// It modifies the arguments and returns the address.
///
/// `let n = parse_int(string="10test4")` -> n=10, string="test4"
///
/// # Arguments
///
/// * `string` - remain of the input command after parsing address
///
/// # Errors
///
/// Returns an error if parsing fails
fn parse_int(string: &mut String) -> Result<usize, Error> {
    let offset = string
        .find(|c: char| !c.is_numeric())
        .unwrap_or(string.len());
    let addr_str: String = string.drain(..offset).collect();
    addr_str.parse::<usize>().map_err(Error::ParseInt)
}

/// Extract the first and the second address of the input and return
/// the number of addresses found
/// It also modifies the Address and the Error structs.
/// The input is truncated after the second command.
///
/// # Arguments
///
/// * `red` - structure with addresses and error message
/// * `input` - input command from the prompt
///
/// # Errors
///
/// Returns an error if the address is invalid
pub fn extract_addresses(red: &mut Red, input: &mut String) -> Result<i8, Error> {
    let mut first = true;

    red.addresses.first_addr = None;
    red.addresses.second_addr = None;

    loop {
        let n: usize;
        // since the input contains at least the newline, it should be
        // guaranteed that input is never empty.
        let c = input
            .chars()
            .next()
            .ok_or(Error::Generic("Invalid input".to_string()))?;
        if c.is_numeric() {
            n = parse_int(input)?;
            if first {
                first = false;
                red.addresses.set_second_addr(Address::new(n));
            } else {
                red.addresses.second_addr = red.addresses.second_addr.map(|a| a.increment_by(n));
            }
        } else {
            match c {
                '\t' | ' ' => {
                    input.remove(0);
                }
                '+' | '-' => {
                    if first {
                        first = false;
                        red.addresses.second_addr = Some(red.addresses.current_addr());
                    }
                    if input.chars().nth(1).ok_or(Error::EmptyInput)?.is_numeric() {
                        input.remove(0);
                        n = parse_int(input)?;
                        if c == '+' {
                            red.addresses.increment_second_addr(n);
                        } else {
                            red.addresses.decrement_second_addr(n);
                        }
                    } else {
                        input.remove(0);
                        if c == '+' {
                            red.addresses.increment_second_addr(1);
                        } else {
                            red.addresses.decrement_second_addr(1);
                        }
                    }
                }
                '.' | '$' => {
                    if !first {
                        red.set_error_msg(Error::InvalidAddress);
                        return Err(Error::InvalidAddress);
                    }
                    first = false;
                    input.remove(0);
                    red.addresses.second_addr = if c == '.' {
                        Some(red.addresses.current_addr())
                    } else {
                        Some(red.addresses.last_addr())
                    }
                }
                '/' | '?' => {
                    if !first {
                        red.set_error_msg(Error::InvalidAddress);
                        return Err(Error::InvalidAddress);
                    }
                    red.addresses.second_addr = Some(regexp::match_addr(red, input, '/' == c)?);
                    if c == input.chars().next().ok_or(Error::EmptyInput)? {
                        input.remove(0);
                    }
                    first = false;
                }
                '%' | ',' | ';' => {
                    if first {
                        if red.addresses.first_addr.is_none() {
                            red.addresses.first_addr = if c == ';' {
                                Some(red.addresses.current_addr())
                            } else {
                                Some(Address::new(1))
                            };
                            red.addresses.second_addr = Some(red.addresses.last_addr());
                        }
                    } else {
                        if red.addresses.second_addr.is_none()
                            || red.addresses.second_addr > Some(red.addresses.last_addr())
                        {
                            red.set_error_msg(Error::InvalidAddress);
                            return Err(Error::InvalidAddress);
                        }
                        if c == ';' {
                            // I need tmp otherwise red is borrow both
                            // mutable and immutable
                            let tmp = red.addresses.second_addr.ok_or(Error::InvalidAddress)?;
                            red.addresses.set_current_addr(tmp);
                        }
                        red.addresses.first_addr = red.addresses.second_addr;
                        first = true;
                    }
                    input.remove(0);
                }
                _ => {
                    if !first
                        && (red.addresses.second_addr.is_none()
                            || red.addresses.second_addr > Some(red.addresses.last_addr()))
                    {
                        red.set_error_msg(Error::InvalidAddress);
                        return Err(Error::InvalidAddress);
                    }
                    let addr_count = if red.addresses.second_addr.is_some() {
                        if red.addresses.first_addr.is_some() {
                            2
                        } else {
                            1
                        }
                    } else {
                        0
                    };
                    if addr_count <= 0 {
                        red.addresses.second_addr = Some(red.addresses.current_addr());
                    }
                    if addr_count <= 1 {
                        red.addresses.first_addr = red.addresses.second_addr;
                    }
                    return Ok(addr_count);
                }
            }
        }
    }
}

/// Parses the input command to find the third address.
///
/// # Arguments
///
/// * `red` - structure with addresses and error message
/// * `input` - command input
///
/// # Return
/// * `Result<Option<usize>, String>` - third address Result
fn get_third_addr(red: &mut Red, input: &mut String) -> Result<Option<Address>, Error> {
    let old1 = red.addresses.first_addr;
    let old2 = red.addresses.second_addr;
    let addr_cnt = extract_addresses(red, input)?;

    if addr_cnt == 0 {
        red.set_error_msg(Error::MissingDestination);
        return Err(Error::MissingDestination);
    }
    if red.addresses.second_addr.is_none()
        || red.addresses.second_addr > Some(red.addresses.last_addr())
    {
        red.set_error_msg(Error::InvalidAddress);
        return Err(Error::InvalidAddress);
    }
    let addr = Ok(red.addresses.second_addr);
    red.addresses.first_addr = old1;
    red.addresses.second_addr = old2;
    addr
}

/// Enumeration for return codes of commands.
pub enum ReturnCommand {
    /// Successful command
    Done,
    /// Quit program after command
    Quit,
    /// Command returns an error
    Error,
    /// Ask the user to imput a command
    ProvideCommand,
    /// Signal user that the command does not exists
    NoAvailableCommand(char),
}

/// Get the command from the input string and execute it given the
/// Addresses and Errors.
///
/// # Arguments
///
/// * `red` - structure with addresses and error message
/// * `input` - command input
pub fn exec_command(red: &mut Red, input: &mut String) -> ReturnCommand {
    let addr_count = match extract_addresses(red, input) {
        Ok(cnt) => cnt,
        Err(_) => return ReturnCommand::Error,
    };

    let c = if let Some(character) = input.chars().next() {
        character
    } else {
        return ReturnCommand::ProvideCommand;
    };
    if !input.is_empty() {
        input.remove(0);
    }

    let cmd = match c {
        '\t' | ' ' => {
            input.remove(0);
            Empty::new().execute(red)
        }
        'a' => insert_lines(red, true),
        'i' => insert_lines(red, false),
        'c' => substitute_lines(red, addr_count),
        'd' => delete_lines(red, addr_count),
        'h' => print_error(red),
        'm' => move_lines(red, input, addr_count),
        'n' => print_lines(red, true, addr_count),
        'p' => print_lines(red, false, addr_count),
        'P' => switch_prompt(red),
        'q' => {
            return quit(red);
        }
        't' => copy_lines(red, input, addr_count),
        'w' => write(red),
        'x' => put_lines(red),
        'y' => yank_lines(red, addr_count),
        '\n' => print_next_line(red, addr_count),
        _ => {
            return ReturnCommand::NoAvailableCommand(c);
        }
    };
    if cmd.is_err() {
        return ReturnCommand::Error;
    }
    ReturnCommand::Done
}

fn insert_lines(red: &mut Red, append: bool) -> Result<(), Error> {
    InsertLine::new(
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
        append,
    )
    .execute(red)
}

fn substitute_lines(red: &mut Red, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    DeleteLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
    )
    .execute(red)?;
    // if current address is the last line the line should be
    // appended and not inserted.
    let append = red.addresses.second_addr == Some(red.addresses.last_addr());
    InsertLine::new(
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
        append,
    )
    .execute(red)?;
    red.addresses
        .set_last_addr(Address::new(red.file_buffer.len()));
    Ok(())
}

fn delete_lines(red: &mut Red, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    DeleteLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
    )
    .execute(red)
}

fn print_error(red: &mut Red) -> Result<(), Error> {
    PrintError::new().execute(red)
}

fn move_lines(red: &mut Red, input: &mut String, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    let addr = get_third_addr(red, input)?;
    if addr >= red.addresses.first_addr && addr < red.addresses.second_addr {
        red.set_error_msg(Error::InvalidDestination);
        Err(Error::InvalidDestination)
    } else {
        MoveLines::new(
            red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
            red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
            addr.ok_or(Error::InvalidAddress)?,
        )
        .execute(red)
    }
}

fn print_lines(red: &mut Red, numbers: bool, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    PrintLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
        numbers,
    )
    .execute(red)
}

/// Turn on and off the prompt
fn switch_prompt(red: &mut Red) -> Result<(), Error> {
    SwitchPrompt::new().execute(red)
}

fn print_next_line(red: &mut Red, addr_count: i8) -> Result<(), Error> {
    // TODO fix this command
    let next_line = red.addresses.current_addr().next();
    red.check_second_addr(next_line, addr_count)?;
    PrintLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
        false,
    )
    .execute(red)
}

fn copy_lines(red: &mut Red, input: &mut String, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    let addr = get_third_addr(red, input)?;
    CopyLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
        addr.ok_or(Error::InvalidAddress)?,
    )
    .execute(red)
}

fn put_lines(red: &mut Red) -> Result<(), Error> {
    if red.addresses.second_addr.is_none()
        || red.addresses.second_addr > Some(red.addresses.last_addr())
    {
        red.set_error_msg(Error::InvalidDestination);
        return Err(Error::InvalidDestination);
    }
    PutLines::new(red.addresses.second_addr.ok_or(Error::InvalidAddress)?).execute(red)
}

fn yank_lines(red: &mut Red, addr_count: i8) -> Result<(), Error> {
    red.check_addr_range2(addr_count)?;
    YankLines::new(
        red.addresses.first_addr.ok_or(Error::InvalidAddress)?,
        red.addresses.second_addr.ok_or(Error::InvalidAddress)?,
    )
    .execute(red)
}

fn write(red: &mut Red) -> Result<(), Error> {
    WriteToBuffer::new().execute(red)
}

fn quit(red: &Red) -> ReturnCommand {
    return if red.is_modified {
        println!("Buffer modified. Do you want to quit? (y/n)");
        let answer = match iofs::get_input() {
            Ok(a) => a,
            Err(e) => {
                println!("{}", e);
                return ReturnCommand::Error;
            }
        };
        match answer.as_ref() {
            "y\n" => ReturnCommand::Quit,
            _ => ReturnCommand::Done,
        }
    } else {
        ReturnCommand::Quit
    };
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn input_parsing() {
        let mut red = Red::default();
        red.addresses.set_last_addr(Address::new(100));
        red.addresses.set_current_addr(Address::new(80));
        // test single address
        let mut input = String::from("50m");
        assert_eq!(extract_addresses(&mut red, &mut input), Ok(1));
        assert_eq!(
            (red.addresses.first_addr, red.addresses.second_addr),
            (Some(Address::new(50)), Some(Address::new(50)))
        );
        // test two addresses
        input = String::from("22,32p33");
        assert_eq!(extract_addresses(&mut red, &mut input), Ok(2));
        assert_eq!(
            (red.addresses.first_addr, red.addresses.second_addr),
            (Some(Address::new(22)), Some(Address::new(32)))
        );
        // test without addresses
        input = String::from(",p");
        assert_eq!(extract_addresses(&mut red, &mut input), Ok(2));
        assert_eq!(
            (red.addresses.first_addr, red.addresses.second_addr),
            (Some(Address::new(1)), Some(Address::new(100)))
        );
        // test blanks
        input = String::from(" 4,5\tp");
        assert_eq!(extract_addresses(&mut red, &mut input), Ok(2));
        assert_eq!(
            (red.addresses.first_addr, red.addresses.second_addr),
            (Some(Address::new(4)), Some(Address::new(5)))
        );
        // test errors
        input = String::from("101p");
        assert_eq!(
            extract_addresses(&mut red, &mut input),
            Err(Error::InvalidAddress)
        );
        input = String::from("5,101p");
        assert_eq!(
            extract_addresses(&mut red, &mut input),
            Err(Error::InvalidAddress)
        );
        // addresses.first_addr > addresses.second_addr is not checked by extract_red()
        input = String::from("5,4p");
        assert_eq!(extract_addresses(&mut red, &mut input), Ok(2));
    }

    #[test]
    fn test_get_third_addr() {
        let mut red = Red::default();
        red.addresses.set_last_addr(Address::new(100));
        red.addresses.set_current_addr(Address::new(80));
        // test third address
        let mut input = String::from("50,84m23\n");
        let _ = extract_addresses(&mut red, &mut input);
        input.remove(0); // emulate command parsing
        let addr = get_third_addr(&mut red, &mut input);
        assert_eq!(addr, Ok(Some(Address::new(23))));
        // check also the first and second addresses correctness
        assert_eq!(
            (red.addresses.first_addr, red.addresses.second_addr),
            (Some(Address::new(50)), Some(Address::new(84)))
        );
        // test missing third address
        input = String::from("50,84m\n");
        let _ = extract_addresses(&mut red, &mut input);
        input.remove(0); // emulate command parsing
        let _addr = get_third_addr(&mut red, &mut input);
        assert_eq!(*red.get_error_msg(), Some(Error::MissingDestination));
        // test out of file third address
        input = String::from("50,84m110\n");
        let _ = extract_addresses(&mut red, &mut input);
        input.remove(0); // emulate command parsing
        let _addr = get_third_addr(&mut red, &mut input);
        assert_eq!(*red.get_error_msg(), Some(Error::InvalidAddress));
    }

    #[test]
    fn test_parse_int1() {
        let mut string = "10test3".to_string();
        let n = parse_int(&mut string);
        assert_eq!((n, string), (Ok(10), "test3".to_string()));
    }

    #[test]
    fn test_parse_int2() {
        let mut string = "test3".to_string();
        let n = parse_int(&mut string);
        assert_eq!(
            (n, string),
            (
                Err(Error::ParseInt("".parse::<usize>().unwrap_err())),
                "test3".to_string()
            )
        );
    }

    #[test]
    fn test_parse_int3() {
        let mut string = "103".to_string();
        let n = parse_int(&mut string);
        assert_eq!((n, string), (Ok(103), "".to_string()));
    }

    #[test]
    fn test_error_msg() {
        let mut red = Red::default();
        assert!(red.get_error_msg().is_none());
        red.set_generic_error("error");
        assert_eq!(
            *red.get_error_msg(),
            Some(Error::Generic("error".to_string()))
        );
    }

    #[test]
    fn test_invalid_address() {
        let mut red = Red::default();
        red.set_error_msg(Error::InvalidAddress);
        assert_eq!(*red.get_error_msg(), Some(Error::InvalidAddress));
    }

    #[test]
    fn test_inc_current_addr() {
        let mut red = Red::default();
        red.addresses.set_last_addr(Address::new(10));
        let new_addr = red.addresses.inc_current_addr();
        assert_eq!(new_addr, Address::new(1));
        red.addresses.set_current_addr(Address::new(10));
        let new_addr2 = red.addresses.inc_current_addr();
        assert_eq!(new_addr2, Address::new(10));
    }
}
